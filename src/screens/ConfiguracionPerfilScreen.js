import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { TabNavigator, TabBarBottom } from "react-navigation";

import DatosPerfil from '../components/DatosPerfil';
import ConfiguracionForm from '../components/ConfiguracionForm';

export default class ConfiguracionPerfilScreen extends React.Component {
  render() {
    return (
      <View>
        <DatosPerfil/>
        <ConfiguracionForm/>
      </View>
    );
  }
}