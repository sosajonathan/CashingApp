import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { withNavigation } from 'react-navigation';

class ReporteScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Ionicons style={styles.logo} name="ios-alert" size={150} color="#E22A35" />
          <Text style={styles.title}>Por favor indícanos por qué motivo quieres reportar este usuario o contenido</Text>
          <View style={styles.buttomsContainer}>
            <TouchableOpacity 
              style={styles.buttomContainer}
              onPress={()=>{this.props.navigation.navigate('ReporteExitoso')}}>
              <Text style={styles.buttomText}>Spam</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.buttomsContainer}>
            <TouchableOpacity 
              style={styles.buttomContainer}
              onPress={()=>{this.props.navigation.navigate('ReporteExitoso')}}>
            <Text style={styles.buttomText}>Contenido inapropiado</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.buttomsContainer}>
            <TouchableOpacity 
              style={styles.buttomContainer}
              onPress={()=>{this.props.navigation.navigate('ReporteExitoso')}}>
            <Text style={styles.buttomText}>Fraude o robo</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.buttomsContainerCancelar}>
            <TouchableOpacity 
              style={styles.buttomContainerCancelar}
              onPress={()=>{this.props.navigation.goBack()}}>
            <Text style={styles.buttomText}>Cancelar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}


export default withNavigation(ReporteScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAF7FA',
  },
  logoContainer: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    margin: 10
  },
  title:{
    color: '#E22A35',
    margin: 10,
    width: 300,
    textAlign: 'center',
    opacity: 0.8
  },
  buttomsContainer: {
    flexDirection: 'row',
    marginTop: 10
  },
  buttomContainer: {
    flex: 1,
    borderRadius:15,
    backgroundColor: '#E22A35',
    paddingVertical: 10,
  },
  buttomText: {
    textAlign: 'center',
    color: '#FAF7FA',
    backgroundColor: 'transparent'
  },
  buttomsContainerCancelar: {
    flexDirection: 'row',
    marginTop: 10
  },
  buttomContainerCancelar: {
    flex: 1,
    borderRadius:15,
    backgroundColor: 'rgba(226,42,53,0.7)',
    paddingVertical: 12,
  }
});