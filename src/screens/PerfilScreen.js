import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { TabNavigator, TabBarBottom } from "react-navigation";
import { StackNavigator, SwitchNavigator, createStackNavigator } from 'react-navigation';

import DatosPerfilConfiguracion from '../components/DatosPerfilConfiguracion';
import PreviewVideos from '../components/PreviewVideos';
import ProfileUser from '../components/ProfileComponents/ProfileUser';

export default class PerfilScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <DatosPerfilConfiguracion/>
        <ScrollView>
          <PreviewVideos/>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
	container: {
    flex: 1,
	},
});