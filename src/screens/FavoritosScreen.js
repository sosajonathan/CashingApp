import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native';
import { TabNavigator, TabBarBottom } from "react-navigation";

import Header from '../components/Header';
import ContentFavorites from '../components/ContentFavorites';
import TransmissionBuyer from '../components/TransmissionComponents/TransmissionBuyer';
import NotificationBuyer from '../components/NotificationComponents/NotificationBuyer';
import NotificationSeller from '../components/NotificationComponents/NotificationSeller';
import ConversationChat from '../components/ChatsComponents/ConversationChat';
import BuySell from '../components/BuySellComponents/BuySell';

export default class FavoritosScreen extends React.Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <Header title="Compra y Venta" />
        {/*<ContentFavorites />*/}
        {/*<Buy />*/}
        {/*<TransmissionSeller />*/}
        {/*<TransmissionBuyer />*/}
        {/*<NotificationBuyer />*/}
        {/*<NotificationSeller />*/}
        {/*<ConversationChat />*/}
        <BuySell />
      </View>
    );
  }
}