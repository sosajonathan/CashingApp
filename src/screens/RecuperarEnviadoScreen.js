import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { withNavigation } from 'react-navigation';
 
class RecuperarEnviadoScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Ionicons style={styles.logo} name="md-information-circle" size={150} color="#E22A35" />
          <Text style={styles.title}>Te hemos enviado un correo electrónico para reestablecer tu contraseña</Text>
          <View style={styles.buttomsContainer}>
            <TouchableOpacity 
              style={styles.buttomContainer}
              onPress={()=>{this.props.navigation.navigate('Login')}}>
              <Text style={styles.buttomText}>Continuar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default withNavigation(RecuperarEnviadoScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAF7FA',
  },
  logoContainer: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    margin: 30
  },
  title:{
    color: '#E22A35',
    margin: 30,
    width: 200,
    textAlign: 'center',
    opacity: 0.8
  },
  buttomsContainer: {
    flexDirection: 'row',
    marginTop: 30
  },
  buttomContainer: {
    flex: 1,
    borderRadius:15,
    backgroundColor: '#E22A35',
    paddingVertical: 12,
  },
  buttomText: {
    textAlign: 'center',
    color: '#FAF7FA',
    backgroundColor: 'transparent'
  },
});