import React from 'react';
import { StyleSheet, Alert, Text, View, TextInput, TouchableOpacity, AsyncStorage } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { withNavigation } from 'react-navigation';
import Expo from 'expo';

//ID para Facebook
const idFacebook = "1511215645667697";
const localStorage = '@MyStore:SD';

class FacebookLoginButton extends React.Component {

  render() {
    return (
        <TouchableOpacity 
          style={styles.buttomFacebookContainer}
          onPress={() => this.loginFacebook()}>
          <Ionicons style={styles.icono} name="logo-facebook" size={20} color="#3b5998" />
          <Text style={styles.buttomTextFacebook}>Iniciar sesión con Facebook</Text>
        </TouchableOpacity>
    );
  }

  loginFacebook = async() =>{
    const {type, token} = await Expo.Facebook.logInWithReadPermissionsAsync(idFacebook, {permissions: ['public_profile','email','user_friends']});
     if (type === 'success'){
      const response = await fetch(`https://graph.facebook.com/me?access_token=${token}&fields=id,name,email,about,picture`);
      const json = await response.json();
      fetch('http://10.0.0.107/cashingAppWebApi/public/api/user', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          name: json.name.toLowerCase(),
          email: json.email.toLowerCase(),
          picture: json.picture.data.url,
          token: token,
        }),
      }).then((response) => response.json())
          .then((responseJson) => {
            console.warn(responseJson);
          })
          .catch((error) => {
            console.warn(error);
          });
          try{
            await AsyncStorage.setItem(localStorage, JSON.stringify({
              email: json.email.toLowerCase(),
              token: token,
              status: 'true',
            }));
          }catch (error) {
            console.log('Error al guardar en el storage');
          }
          this.props.navigation.navigate('SesionExitosa');
    }else{
      alert(type);
    }
  }
}

export default withNavigation(FacebookLoginButton);

const styles = StyleSheet.create({
  buttomsContainer: {
    flexDirection: 'row'
  },
  buttomFacebookContainer: {
    borderRadius:15,
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    marginTop: 8,
    backgroundColor: 'transparent',
    paddingVertical: 5,
  },
  icono: {
    marginRight:5
  },
  buttomTextFacebook: {
    textAlign: 'center',
    color: '#3b5998',
    backgroundColor: 'transparent'
  }
});