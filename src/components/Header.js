import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class Header extends React.Component {
  render() {
  	
  	const { containerHeader, textHeader } = styles;

    return (
    	<View style={containerHeader}>
      		<Text style={textHeader}>{this.props.title}</Text>
      	</View>
    );
  }
}

const styles = StyleSheet.create({
	containerHeader: {
		backgroundColor: '#E22A35',
		justifyContent: 'center',
		alignItems: 'center',
		height: 50,
		padding: 7,
		shadowColor: '#000',
		shadowOffset: {width: 0, height: 1},
		shadowOpacity: 1,
		elevation: 2,
		position: 'relative',
	},
	textHeader: {
		fontSize: 25,
		color: 'white'
	}
});