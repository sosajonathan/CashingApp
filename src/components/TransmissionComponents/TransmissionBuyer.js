import React from 'react';
import { StyleSheet, View, Image, TextInput, KeyboardAvoidingView, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { withNavigation } from 'react-navigation';

import Comments from './Comments';
class TransmissionBuyer extends React.Component{
	render(){
		return(
			<KeyboardAvoidingView style={{flex: 1}} behavior="padding" enabled>
			<View style={{flexDirection: 'row', backgroundColor: 'transparent'}}>
				<TouchableOpacity 
					style={styles.buttomContainer}
					onPress={()=>{this.props.navigation.navigate('Comprar')}}>
					<Ionicons style={styles.icono} name="ios-basket" size={30} color="rgba(217,145,65,0.9)" />
				</TouchableOpacity>
				<TouchableOpacity 
					style={styles.buttomContainer}
					onPress={()=>{console.warn('me gusta')}}>
					<Ionicons style={styles.icono} name="ios-heart-outline" size={30} color="rgba(0,0,0,0.5)" />
				</TouchableOpacity>
				<TouchableOpacity 
					style={styles.buttomContainer}
					onPress={()=>{this.props.navigation.navigate('Reportar')}}>
					<Ionicons style={styles.icono} name="ios-alert" size={30} color="rgba(217,145,65,0.9)" />
				</TouchableOpacity>
			</View>
			<Image style={{flex: 1}} source={{ uri: 'https://http2.mlstatic.com/fitness-ejercitador-de-abdominales-y-piernas-tv-compras-D_NQ_NP_831384-MLA26561104291_122017-F.jpg' }} />
			<Comments />
			<Comments />
			<TextInput 
	          style={styles.input}
	          placeholder='Comentario'
	          underlineColorAndroid='transparent'
	          placeholderTextColor='rgba(0,0,0,0.4)'
	        >
	    </TextInput>
		</KeyboardAvoidingView>
		);
	}
}

const styles = StyleSheet.create({
  input: {
    borderRadius:15,
    height: 35,
    backgroundColor: 'rgba(173,81,38,0.2)',
    marginBottom: 3,
    marginHorizontal: 4,
    color: 'rgba(173,81,38,0.9)',
    paddingHorizontal: 10
	},
	buttomContainer: {
		margin: 4,
  },
});

export default withNavigation(TransmissionBuyer);