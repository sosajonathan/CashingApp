import React from 'react';
import { StyleSheet, View, Image, Text } from 'react-native';

const ChatDetail = (props) => {
	const { contentAvatar, containerChat, avatarChat, userChat, fontChat } = styles;

	return (
		<View style={containerChat}>
			<View style={contentAvatar}>
				<Image style={avatarChat} source={{ uri: props.chat.thumbnail_image }}/>
				<Text style={[userChat, fontChat]}>{props.chat.title}</Text>
			</View>

			<View style={userChat}>
				<Text style={fontChat}>15:00</Text>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	containerChat: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginBottom: 25,
		paddingBottom: 10,
		borderBottomWidth: 1,
		borderBottomColor: '#ddd',
		paddingHorizontal: 10
	},
	contentAvatar: {
		flexDirection: 'row',
	},
	avatarChat:{
		height: 60,
		width: 60,
		borderRadius: 50,
		marginRight: 10,
	},
	userChat: {
		alignSelf: 'center',
	},
	fontChat: {
		fontSize: 18,
	}
});

export default ChatDetail;