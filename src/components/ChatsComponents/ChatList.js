import React from 'react';
import { View, Text, FlatList } from 'react-native';

import ChatDetail from './ChatDetail';

class ChatList extends React.Component{

	state = { chats: [] };

	componentWillMount(){
		fetch('https://rallycoding.herokuapp.com/api/music_albums')
	    	.then(response => response.json())
	    	.then(data => this.setState({ chats: data }));
	}

	_keyExtractor = (item, index) => item.title;

	_renderChats = ({item}) => {
		return( 
			<ChatDetail key={item.title} chat={item} /> 
		);
	}

	render() {
		return (
			<View style={{flex:1}}>
				<FlatList
					data={this.state.chats}
					keyExtractor={this._keyExtractor}
					numColumns={1}
					style={{flexDirection: 'column'}}
					renderItem={this._renderChats}
				>
				</FlatList>
			</View>
		);
	}
}

export default ChatList;