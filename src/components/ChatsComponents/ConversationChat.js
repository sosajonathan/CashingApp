import React from 'react';
import { StyleSheet, View, Text, Image, ScrollView, TextInput, KeyboardAvoidingView, TouchableOpacity } from 'react-native';

import { Ionicons } from '@expo/vector-icons';


export default class ConversationChat extends React.Component{
	render() {
		return(
			<KeyboardAvoidingView style={{flex: 1, paddingHorizontal: 10 }} behavior="padding" enabled>
				<ScrollView>
					<View style={{flexDirection: 'row', justifyContent: 'flex-start', marginVertical: 10}}>
						<Image style={{height: 40, width: 40, borderRadius: 50, marginHorizontal: 4}} source={ require('../../imgs/avatar-default.png') }/>
						<View style={{ backgroundColor: 'rgba(0,0,0,0.2)', paddingVertical: 10, paddingHorizontal: 5, borderRadius: 5, flex: 1 }}>
							<Text style={{fontSize: 17}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
							sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
							</Text>
						</View>
					</View>
					<View style={{flexDirection: 'row', justifyContent: 'flex-end', marginVertical: 10}}>
						<View style={{ backgroundColor: 'rgba(173,81,38,0.2)', paddingVertical: 10, paddingHorizontal: 5, borderRadius: 5, flex: 1 }}>
							<Text style={{fontSize: 17}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
							sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
							</Text>
						</View>
						<Image style={{height: 40, width: 40, borderRadius: 50, marginHorizontal: 4}} source={ require('../../imgs/avatar-default.png') }/>
					</View>
				</ScrollView>
				<View style={{ flexDirection: 'row', marginVertical: 5, marginHorizontal: 4}}>
					<TextInput 
			          style={styles.input}
			          placeholder='Escribe el mensaje'
			          underlineColorAndroid='transparent'
			          placeholderTextColor='rgba(0,0,0,0.4)'
			        >
			        </TextInput>
			        <TouchableOpacity style={{paddingHorizontal: 12, paddingVertical: 8, borderRadius: 50, marginLeft: 3, backgroundColor: 'rgba(217,145,65,0.9)'}}>
						<Ionicons name="md-send" style={{fontSize: 32, color: 'white', fontWeight: 'bold'}} />			        
			    	</TouchableOpacity>
			    </View>
			</KeyboardAvoidingView>
		);
	}
}

const styles = StyleSheet.create({
  input: {
  	flex: 1,
    borderRadius:15,
    height: 50,
    backgroundColor: 'rgba(173,81,38,0.2)',
    color: 'rgba(173,81,38,0.9)',
    paddingHorizontal: 10
  }
});