import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';

import { Ionicons } from '@expo/vector-icons';

const NotificationBuyer = () => {
	const { 
        containerButtons, 
        button, 
        buttonText, 
        colorLight, 
        colorDark,
    } = styles;
	return (
		<View style={{flex: 1}}>
			<View style={{flex: 3, justifyContent: 'center', alignItems: 'center'}}>
				<Ionicons name="md-warning" style={{fontSize: 90, marginBottom: 20}} />
				<Text style={{ textAlign: 'center', marginBottom: 6, fontSize: 18 }}>Ud ha realizado la compra de X producto.</Text>
				<Text style={{ textAlign: 'center', marginBottom: 6, fontSize: 18 }}>Confirme que ha recibido el producto exitosamente.</Text>
			</View>
			<View style={containerButtons}>
				<TouchableOpacity style={[button, colorLight]} >
		        	<Text style={buttonText}>Continuar</Text>
		    	</TouchableOpacity>

		    	<TouchableOpacity style={[button, colorDark]} >
		        	<Text style={buttonText}>Cancelar</Text>
		    	</TouchableOpacity>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
    containerButtons: { 
    	flex: 1,
        flexDirection: 'row', 
        justifyContent: 'center', 
        marginBottom: 12, 
        paddingHorizontal: 5, 
        alignItems: 'flex-end'
    },
	button: {
		flex: 1,
		alignItems: 'center',
		padding: 10,
		borderRadius: 15
	},
	colorLight: {
		backgroundColor: '#D99141',
		marginRight: 10
	},
	colorDark: {
		backgroundColor: '#B95B22',
	},
	buttonText: {
		color: 'white',
		fontWeight: 'bold'
	}
});

export default NotificationBuyer;