import React from 'react';
import { View, Text, FlatList } from 'react-native';

import BuySellDetail from './BuySellDetail';

class BuySellList extends React.Component{

	state = { buySell: [] };

	componentWillMount(){
		fetch('https://rallycoding.herokuapp.com/api/music_albums')
	    	.then(response => response.json())
	    	.then(data => this.setState({ buySell: data }));
	}; 

	_keyExtractor = (item, index) => item.title;

	_renderBuySell = ({item}) => {
		return( 
			<View>
				<BuySellDetail key={item.title} chat={item} />
			</View>
		);
	}

	render() {
		return (
			<View style={{flex:1}}>
				<FlatList
					data={this.state.buySell}
					keyExtractor={this._keyExtractor}
					numColumns={1}
					style={{flexDirection: 'column'}}
					renderItem={this._renderBuySell}
				>
				</FlatList>
			</View>
		);
	}
}

export default BuySellList;