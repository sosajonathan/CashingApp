import React from 'react';
import { StyleSheet, View, Image, Text, TouchableOpacity, Alert } from 'react-native';

import Swipeout from 'react-native-swipeout';

const BuySellDetail = (props) => {
	const { containerBuySell, contentBuySell, fontBuySell, colorGreen, colorBlue } = styles;

  state = {
    modalVisible: false,
	};

	setModalVisible = ({visible}) => {
    this.setState({modalVisible: visible});
  }
	
	// Buttons
	var swipeoutBtns = [
	  {
	    text: 'Button'
	  }
	]
	return (
		<View>
			<Swipeout right={swipeoutBtns}>
				<TouchableOpacity style={[containerBuySell, colorGreen]}
					onPress={() => {Alert.alert(
						'Venta realizada',
						'Detalles de la venta',
						[
							{text: 'Confirmar venta', onPress: () => console.log('Ask me later pressed')},
							{text: 'Regresar', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
						],
						{ cancelable: false }
					)}}>
					<Text style={[contentBuySell, fontBuySell]}>{props.chat.title}</Text>
					<Text style={fontBuySell}>20 $</Text>
				</TouchableOpacity>
			</Swipeout>

			<Swipeout right={swipeoutBtns}>
				<TouchableOpacity style={[containerBuySell, colorBlue]}
					onPress={() => {Alert.alert(
						'Compra realizada',
						'Detalles de la compra',
						[
							{text: 'Confirmar compra', onPress: () => console.log('Ask me later pressed')},
							{text: 'Regresar', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
						],
						{ cancelable: false }
					)}}>
					<Text style={[contentBuySell, fontBuySell]}>{props.chat.title}</Text>
					<Text style={fontBuySell}>20 $</Text>
				</TouchableOpacity>
			</Swipeout>
		</View>
	);
}

const styles = StyleSheet.create({
	containerBuySell: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingVertical: 20,
		borderBottomWidth: 1,
		borderBottomColor: '#ddd',
		paddingHorizontal: 10
	},
	contentBuySell: {
		alignSelf: 'center',
	},
	fontBuySell: {
		fontSize: 18,
	},
	colorBlue: {
		backgroundColor: '#4682b4'
	},
	colorGreen: {
		backgroundColor: '#3cb371'
	}
});

export default BuySellDetail;