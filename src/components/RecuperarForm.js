import React from 'react';
import { StyleSheet, Modal, ActivityIndicator, Alert, Text, View, TextInput, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { withNavigation } from 'react-navigation';

const url = 'http://10.0.0.107/cashingAppWebApi/public/api/auth/recovery-password';

class RecuperarForm extends React.Component {
  
  constructor(props) {
    super(props)
      this.state = {
        correo: '',
        loading: false,
      }

    this._recoveryPassword = this._recoveryPassword.bind(this);
  }

    /* Conexión con la api para recuperación de contraseña */
  async _recoveryPassword(){
      this.setState({
        loading: true
      });

      await fetch(url, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email: this.state.correo.toLowerCase(),
        }),
      }).then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.recovery == true) {
            this.setState({
              loading: false
            });
            this.props.navigation.navigate('RecuperarEnviado')
          }else{
            Alert.alert(
              '¿Estas seguro de tu correo?',
              'Verifica que tu correo fue escrito correctamente',
              [
                {text: 'Entendido', onPress: () => console.log('onpress')},
              ],
              { cancelable: false }
            )
          }
        })
      .catch((error) => {
        console.warn(error);
      });
  }

  render() {
    return (
      <View style={styles.container}>
        {this.state.loading &&
          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.loading}
          >
            <View style={styles.modalDialog}>
              <ActivityIndicator 
                size="large" 
                color="#F44336" 
                animating={true}
                style={{alignSelf: 'center'}} 
              />
            </View>
          </Modal>
        }
        <TextInput 
          style={styles.input}
          placeholder='Correo electrónico'
          underlineColorAndroid='transparent'
          placeholderTextColor='rgba(226,42,53,0.4)'
          returnKeyType='next'
          keyboardType='email-address'
          autoCorrect={false}
          onChangeText={(value) => this.setState({correo: value})}
        ></TextInput>
        <View style={styles.buttomsContainer}>
          <TouchableOpacity 
            style={styles.buttomContainerEnviar}
            onPress={this._recoveryPassword}
            disabled={this.state.loading}
            >
            <Text style={styles.buttomText}>Enviar correo de verificación</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.buttomContainerCancelar}
            onPress={()=>{this.props.navigation.navigate('Login')}}
            disabled={this.state.loading}
            >
            <Text style={styles.buttomText}>Cancelar</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default withNavigation(RecuperarForm);

const styles = StyleSheet.create({
  container: {
    padding: 10
  },
  input: {
    borderRadius:15,
    height: 35,
    backgroundColor: 'rgba(226,42,53,0.2)',
    marginBottom: 10,
    color: 'rgba(226,42,53,0.9)',
    paddingHorizontal: 10
  },
  buttomText: {
    textAlign: 'center',
    color: '#FAF7FA',
    backgroundColor: 'transparent'
  },
  buttomsContainer: {
    flexDirection: 'row'
  },
  buttomContainerEnviar: {
    borderRadius:15,
    flex: 1,
    marginTop: 5,
    marginRight: 5,
    marginBottom: 5,
    backgroundColor: 'rgba(226,42,53,0.9)',
    paddingVertical: 10,
  },
  buttomContainerCancelar: {
    borderRadius:15,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
    marginRight: 5,
    marginBottom: 5,
    backgroundColor: 'rgba(226,42,53,0.7)',
    paddingVertical: 10,
  },  
  modalDialog: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.4)'
  }
});