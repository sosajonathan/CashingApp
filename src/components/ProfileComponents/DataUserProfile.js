import React from 'react';
import { StyleSheet, View, Image, Text } from 'react-native';

const DataUserProfile = () => {

	const { containerData, avatarUser, contentDataUser, inputText } = styles;

	return (
		<View style={containerData}>
			<Image style={avatarUser} source={ require('../../imgs/avatar-default.png') } />
			<View style={contentDataUser}>
				<Text style={inputText}>Nombre de Usuario</Text>
				<Text style={inputText}>Reputación</Text>
			</View>
		</View>
	);

};

const styles = StyleSheet.create({
	containerData:{
		paddingLeft: 12,
		paddingVertical: 10,
		flexDirection: 'row',
		borderWidth: 1,
		borderColor: '#ddd',
		marginBottom: 1,
		backgroundColor: '#FAF7FA'
	},
	avatarUser: {
		height: 70,
		width: 70,
		borderRadius: 50,
		justifyContent: 'center',
	},
	contentDataUser: {
		flexDirection: 'column',
		justifyContent: 'space-around',
		paddingLeft: 8
	},
	inputText: {
		fontSize: 17,
	}
});

export default DataUserProfile;