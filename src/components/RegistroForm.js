import React from 'react';
import { Alert, StyleSheet, Modal, Text, View, ActivityIndicator, TextInput, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { withNavigation } from 'react-navigation';

import FacebookLoginButton from './APISocialNetwork/FacebookLoginButton';
import GoogleLoginButton from './APISocialNetwork/GoogleLoginButton';

const url = 'http://10.0.0.107/cashingAppWebApi/public/api/auth/user';

class RegistroForm extends React.Component {

constructor(props) {
    super(props)
    this.state = {
      nombre: '',
      correo: '',
      clave: '',
      claveConfirmada: '',
      title: '',
      message: '',
      button: '',
      loading: false
    }

    this._userRegistration = this._userRegistration.bind(this);
    this._alertRegistration = this._alertRegistration.bind(this);
  }

  /* Alert que despliega los distintos tipos de error: Credenciales erradas, 
  Verificación de correo, Conexión a la BD */
  _alertRegistration = (resp) => {
    if (resp.created == 'exists') {
      this.setState({
        title: 'Correo existente',
        message: 'El usuario con el correo ' +resp.email+ ' ya existe',
        button: 'Intentar de nuevo',
        loading: false
      });
    }else if (resp.created == true) {
      this.setState({
        title: 'Usuario creado',
        message: 'El usuario ha sido creado con exito',
        button: 'Ok',
        loading: false
      });
    }else if(resp.errors){
      this.setState({
        title: 'Error en el registro',
        message: resp.errors[0],
        button: 'Intentar de nuevo',
        loading: false
      });
    }
    Alert.alert(
      this.state.title,
      this.state.message,
      [
        {text: this.state.button , onPress: () => {
          if (this.state.button == 'Ok') {
            this.props.navigation.navigate('Login')
          }else{
            console.log('onpress')
          }
        }},
      ],
      { cancelable: false }
    )
  }

    /* Conexión con la api y registro del usuario */
  async _userRegistration(){
      this.setState({
        loading: true
      });

      await fetch(url, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          name: this.state.nombre.toLowerCase(),
          email: this.state.correo.toLowerCase(),
          password: this.state.clave.toLowerCase(),
          password_confirmation: this.state.claveConfirmada.toLowerCase()
        }),
      })
        .then((response) => response.json())
          .then(this._alertRegistration)
      .catch((error) => {
        console.warn(error);
      });
  }

  render() {
    return (
      <View style={styles.container}>
        {this.state.loading &&
            <Modal
              animationType="fade"
              transparent={true}
              visible={this.state.loading}
              onRequestClose={() => {}}
            >
              <View style={styles.modalDialog}>
                <ActivityIndicator 
                  size="large" 
                  color="#F44336" 
                  animating={true}
                  style={{alignSelf: 'center'}} 
                />
              </View>
            </Modal>
        }
        <TextInput 
          style={styles.input}
          placeholder='Nombre y Apellido'
          underlineColorAndroid='transparent'
          placeholderTextColor='rgba(226,42,53,0.4)'
          returnKeyType='next'
          onSubmitEditing={()=>this.correoInput.focus()}
          autoCorrect={false}
          onChangeText={(value) => this.setState({nombre: value})}
        ></TextInput>
        <TextInput 
          style={styles.input}
          placeholder='Correo electrónico'
          underlineColorAndroid='transparent'
          placeholderTextColor='rgba(226,42,53,0.4)'
          returnKeyType='next'
          onSubmitEditing={()=>this.passwordInput.focus()}
          keyboardType='email-address'
          autoCorrect={false}
          ref={(input)=> this.correoInput=input}
          onChangeText={(value) => this.setState({correo: value})}
        ></TextInput>
        <TextInput 
          style={styles.input}
          placeholder='Contraseña'
          underlineColorAndroid='transparent'
          placeholderTextColor='rgba(226,42,53,0.4)'
          returnKeyType='next'
          onSubmitEditing={()=>this.passwordConfirmInput.focus()}
          secureTextEntry
          ref={(input)=> this.passwordInput=input}
          onChangeText={(value) => this.setState({clave: value})}
        ></TextInput>
        <TextInput 
          style={styles.input}
          placeholder='Confirmar contraseña'
          underlineColorAndroid='transparent'
          placeholderTextColor='rgba(226,42,53,0.4)'
          returnKeyType='go'
          secureTextEntry
          ref={(input)=> this.passwordConfirmInput=input}
          onChangeText={(value) => this.setState({claveConfirmada: value})}
        ></TextInput>
        <View style={styles.buttomsContainer}>
          <TouchableOpacity 
            style={styles.buttomRegistroContainer}
            onPress={this._userRegistration}>
            <Text style={styles.buttomText}>Registrarme</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.buttomCancelarContainer}
            onPress={()=>{this.props.navigation.navigate('Login')}}>
            <Text style={styles.buttomText}>Cancelar</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.buttomsContainer}>
          <FacebookLoginButton/>
        </View>
        <View style={styles.buttomsContainer}>
          <GoogleLoginButton/>
        </View>
      </View>
    );
  }
}

export default withNavigation(RegistroForm);

const styles = StyleSheet.create({
  container: {
    padding: 10
  },
  input: {
    borderRadius:15,
    height: 35,
    backgroundColor: 'rgba(226,42,53,0.2)',
    marginBottom: 10,
    color: 'rgba(226,42,53,0.9)',
    paddingHorizontal: 10
  },
  buttomRegistroContainer: {
    borderRadius:15,
    flex: 1,
    marginTop: 5,
    marginRight: 5,
    marginBottom: 5,
    backgroundColor: 'rgba(226,42,53,0.9)',
    paddingVertical: 10,
  },
  buttomCancelarContainer: {
    borderRadius:15,
    flex: 1,
    marginTop: 5,
    marginLeft: 5,
    marginBottom: 5,
    backgroundColor: 'rgba(226,42,53,0.7)',
    paddingVertical: 10,
  },
  buttomText: {
    textAlign: 'center',
    color: '#FAF7FA',
    backgroundColor: 'transparent'
  },
  buttomsContainer: {
    flexDirection: 'row'
  },
  buttomFacebookContainer: {
    borderRadius:15,
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    marginTop: 8,

    backgroundColor: 'transparent',
    paddingVertical: 5,
  },
  buttomGoogleContainer: {
    borderRadius:15,
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    marginTop: 4,
    backgroundColor: 'transparent',
  },
  icono: {
    marginRight:5
  },
  buttomTextFacebook: {
    textAlign: 'center',
    color: '#3b5998',
    backgroundColor: 'transparent'
  },
  buttomTextGoogle: {
    textAlign: 'center',
    color: '#d34836',
    backgroundColor: 'transparent'
  },
  modalDialog: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.4)'
  }
});